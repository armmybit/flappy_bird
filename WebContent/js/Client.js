var _user_id = getParameterByName('id', window.location.href);
var _play_id = getParameterByName('play_id', window.location.href);
var _ts = getParameterByName('ts', window.location.href);
// var gameState = '';

function getParameterByName(n, url) {
	var name = n.replace(/[\[\]]/g, '\\$&');
	var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var Client = {};
Client.game = null;
Client.isDead = false;
Client.playId = _play_id;
User.playId = _play_id;

Client.socket = io.connect('https://coolgame.la/?id=' + _user_id + '&play_id=' + _play_id + '&ts=' + _ts, {
	path : '/socket15'
});

// Client.socket = io.connect('https://coolgame.la/?id=' + _user_id +
// '&play_id=' + _play_id + '&ts=' + _ts, {
// path : '/socket15_test'
// });

Client.socket.on('connect', function() {
	console.log("connected!");
});

Client.socket.on('disconnect', function(data) {
	console.log("disconnected!");
	gameInstance.closeGame();
});

Client.exitByUserRequest = function() {
	var data = {
		idx : _user_id
	};

	Client.socket.emit('exitByUserRequest', data);
};
Client.socket.on('exitByUserResponse', function(data) {
	console.log('[exitByUserResponse]');
	getScene().exitByUserResponse();
});

Client.startGameRequest = function() {
	console.log('user_id: ' + _user_id + ' | ' + 'play_id: ' + _play_id + ' | ' + 'ts: ' + _ts);

	if (gameInstance.testMode) {
		console.log('[TEST][startGameRequest]');
		var data = {
			code : 200
		};
		console.log('[TEST][startGameResponse] ' + JSON.stringify(data));
		getScene().startGameResponse(data);
	} else {
		console.log('[startGameRequest]');
		Client.socket.emit('startGameRequest');
	}
};
Client.socket.on('startGameResponse', function(data) {

	console.log('[startGameResponse] ' + JSON.stringify(data));

	switch (data.code) {
		case 200 :
			getScene().startGameResponse(data);
			break;
		default :
			getScene().errorMessage(data);
			break;
	}
});

Client.updateScoreRequest = function(score) {

	if (!gameInstance.testMode) {
		var data = {
			idx : _user_id,
			score : score
		};
		console.log('[updateScoreRequest] ' + JSON.stringify(data));
		Client.socket.emit('updateScoreRequest', data);
	}

};

// game over
Client.gameOverRequest = function(idx) {
	if (gameInstance.testMode) {
		console.log('[TEST][gameOverRequest]');
		var data = {
			code : 200
		};
		console.log('[TEST][gameOverResponse] ' + JSON.stringify(data));
		getScene().gameOverResponse(data);
	} else {
		var data = {
			idx : _user_id
		};
		console.log('[gameOverRequest] ' + JSON.stringify(data));
		Client.socket.emit('gameOverRequest', data);
	}
};
Client.socket.on('gameOverResponse', function(data) {
	console.log('[gameOverResponse] ' + JSON.stringify(data));
	getScene().gameOverResponse(data);
});

Client.socket.on('serverResponse', function(data) {

	console.log('[serverResponse] ' + JSON.stringify(data));

	switch (data.code) {
		case 5000 :// Server closed
			Game.showMessage(getLang().ERROR_SERVER, true);
			break;
		default :// Server closed
			Game.showMessage(getLang().ERROR_SERVER, true);
			break;
	}
});
