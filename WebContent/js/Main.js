var gameName = 'FLAPPY_BIRD';
var gameInstance = null;
var version = "version 1.0";

var KEYS = {
	LANGUAGE : gameName + '_LANGUAGE',
	BGM_VOLUME : gameName + '_BGM_VOLUME',
	SFX_VOLUME : gameName + '_SFX_VOLUME',
	TUTORIAL : gameName + '_TUTORIAL'
};

window.onload = function() {

	var game = new Phaser.Game(720, 1280, Phaser.CANVAS);

	// Add the States your game has.
	game.state.add("Boot", Boot);
	game.state.add("Menu", Menu);
	game.state.add("Preload", Preload);
	game.state.add("Level", Level);

	game.state.start("Boot");

	gameInstance = game;

	getScene = function() {
		return game.state.getCurrentState();
	};

	// game config
	game.speed = -200;
	game.testMode = false;

	// exit
	game.client = function() {
		var u = navigator.userAgent;
		var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
		var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
		if (isiOS) {
			phone = "ios";
		} else {
			phone = "android";
		}
		return phone;
	};

	game.exit = function() {
		if (game.client() == "android") {
			try {
				android.closeGame();
			} catch ($e) {
				alert($e.name + ": " + $e.message);
			}
		} else {
			window.location.href = 'CloseGame';
			Client.socket.close();
		}
	};

	game.closeGame = function() {
		if (game.client() == "android") {
			try {
				android.closeGame();
			} catch ($e) {
				alert($e.name + ": " + $e.message);
			}
		} else {
			window.location.href = 'CloseGame';
			Client.socket.close();
		}

	};

};
