var langEnum = {
	TH : 'TH',
	EN : 'EN'
};

var langKey = langEnum.TH;
if (localStorage.getItem('LANGUAGE') == null) {
	localStorage.setItem('LANGUAGE', langKey);
} else {
	langKey = localStorage.getItem('LANGUAGE');
}

var langEN = new LangEN();
var langTH = new LangTH();

var lang = langTH;

switch (langKey) {
	case langEnum.EN :
		lang = langEN;
		break;
	case langEnum.TH :
		lang = langTH;
		break;
}

var LM = {
	switchLang : function(key) {
		switch (key) {
			case langEnum.EN :
				lang = langEN;
				break;
			case langEnum.TH :
				lang = langTH;
				break;
		}
	}
};
