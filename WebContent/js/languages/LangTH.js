/**
 * 
 */

// var LaTH = {
// TEXT_A : 'อักษร A',
// TEXT_B : 'อักษร B'
// };
function LangTH() {

	this.SETTINGS = 'ตั้งค่า';
	this.BGM = 'ดนตรี';
	this.SFX = 'เสียง';
	this.LANG = 'ภาษา';

	this.ROOKIE = 'มือใหม่';
	this.DEMON_SLAYER = 'นักล่าอสูร';
	this.HASHIRA = 'เสาหลัก';

	this.COMING_SOON = 'เร็วๆ นี้';
	this.NOT_ENOUGH_POINTS = 'มีแต้มไม่เพียงพอ';

	this.TANJIRO = 'ทันจิโร่';
	this.ZENITSU = 'เซนอิตสึ';

	this.CHARACTER = 'ตัวละคร';
	this.MONSTERS = "มอนสเตอร์";

	this.BONUS = 'โบนัส';
	this.BOSS = 'บอส';

	this.AUTO = 'ออโต้';
	this.ITEM_CODE = 'ไอเทมโค้ด';
	this.ITEM_CODE_HEADER = 'ไอเทมโค้ด';

	this.LUCKY_CODE = 'ลัคกี้โค้ด';
	this.LUCKY_CODE_HEADER = 'ลัคกี้โค้ด';

	this.BONUS_TIME = 'เวลาโบนัส!';

	// btn
	this.SETTING = 'ตั้งค่า';
	this.EXIT = 'ออก';
	this.BACK = 'กลับ';

	this.PANEL_ALERT_HEADER = 'แจ้งเตือน';

	this.PANEL_TUTORIAL_HEADER = 'เรียนรู้';
	this.TUTORIAL_HOW_TO_PLAY = {
		header : 'วิธีเล่น',
		desc : 'ยิงบับเบิ้ลเพื่อทำลาย\nโดยบับเบิ้ลต้องเหมือนกันอย่างน้อย 3 ลูก'
	};
	this.TUTORIAL_ANGRY_GAUGE = {
		header : 'เกจความโกรธ',
		desc : 'เติมเกจความโกรธ\nโดยการทำ combo อย่างน้อย x3'
	};
	this.TUTORIAL_ANGRY_GAUGE_MAX = {
		header : 'โมโหแล้วนะ!',
		desc : 'เมื่อโมโหสุดขีด กดที่สัญลักษณ์โกรธ\nเพื่อรับบับเบิ้ลไฟ ซึ่งจะทำลายพื้นที่\nรอบๆจุดกระทบ'
	};
	this.TUTORIAL_ANGEL_GOOD = {
		header : 'นางฟ้าใจดี',
		desc : 'นางฟ้าใจดี จะนำแต่สิ่งดีๆมาให้เสมอ\nยิงเพื่อรับพร'
	};
	this.TUTORIAL_ANGEL_DARK = {
		header : 'นางฟ้าใจร้าย',
		desc : 'ยิงนางฟ้าใจร้าย!\nเพื่อหยุดการเพิ่มบับเบิ้ลชนิดใหม่'
	};
	this.TUTORIAL_BOOSTER_X2_SCORE = {
		header : 'คะแนนx2',
		desc : 'แต้มที่ได้จากการยิงจะ x2'
	};
	this.TUTORIAL_BOOSTER_AIM_ASSIST = {
		header : 'ช่วยเล็ง',
		desc : 'แสดงเส้นนำทางของบับเบิ้ล'
	};
	this.TUTORIAL_BOOSTER_PUSH = {
		header : 'ผลัก',
		desc : 'ผลักกลุ่มบับเบิ้ลกลับขึ้นไป'
	};

	this.WORLD_RANKING = '100 อันดับแรก';
	this.TOP100_RANKING = '100 อันดับ';

	this.RANKING_REWARDS = 'รางวัลอันดับ';

	this.MY_RANK = 'อันดับของฉัน';

	this.DAILY = 'รายวัน';
	this.WEEKLY = 'รายสัปดาห์';
	this.MONTHLY = 'รายเดือน';

	this.DAILY_RANKING = 'อันดับรายวัน';
	this.WEEKLY_RANKING = 'อันดับรายสัปดาห์';
	this.MONTHLY_RANKING = 'อันดับรายเดือน';

	this.DAILY_RANKING_REWARDS = 'รางวัลรายวัน';
	this.WEEKLY_RANKING_REWARDS = 'รางวัลรายสัปดาห์';
	this.MONTHLY_RANKING_REWARDS = 'รางวัลรายเดือน';

	this.PANEL_GAME_OVER_HEADER = 'จบเกม';

	this.PANEL_RECEIVED_ITEM_HEADER = 'ได้รับ';

	this.PANEL_CONFIRM_PURCHASE_HEADER = 'ยืนยันการซื้อ';
	this.PANEL_CONFIRM_PURCHASE_CONTENT = 'ซื้อ';

	this.PANEL_SETTINGS_HEADER = 'ตั้งค่า';

	this.PANEL_PAUSE_HEADER = 'หยุดชั่วคราว';

	this.PANEL_REDEEM_CODE_HEADER = 'แลกไอเทมโค้ด';

	this.LEADERBOARD = 'อันดับผู้เล่น';

	this.NOT_ENOUGH_HEART = 'คุณมีหัวใจไม่เพียงพอ';
	this.GO_TO_SHOP = 'ไปที่ร้านค้า';

	this.ITEM_INFO_002 = 'เพิ่มเกจโมโหจนเต็ม';
	this.ITEM_INFO_003 = 'หยุดเวลา 5 วินาที';
	this.ITEM_INFO_004 = 'ทำลายบับเบิ้ลทั้งหมด';

	// BTN
	this.PLAY = 'เล่น';
	this.SHOP = 'ร้านค้า';
	this.INVENTORY = 'ช่องเก็บของ';

	this.REDEEM = 'แลก';
	this.REDEEM_CODE = 'แลกโค้ด';
	this.MAIL = 'จดหมาย';
	this.SETTINGS = 'ตั้งค่า';
	this.MUSIC = 'ดนตรี';
	this.SOUND = 'เสียง';
	this.HEART = 'หัวใจ';
	this.ITEM = 'ไอเทม';
	this.CONFIRM = 'ตกลง';
	this.MAX = 'เต็ม';
	this.RANK = 'อันดับ';
	this.BEST = 'ดีที่สุด';
	this.FREE = 'แถมฟรี';
	this.OWN = 'มีอยู่แล้ว';
	this.GET = 'รับ';
	this.EXPIRED = 'หมดอายุ';
	this.CANCEL = 'ยกเลิก';

	this.TIME_D = 'วัน';
	this.TIME_H = 'ชม';
	this.TIME_M = 'นาที';
	this.TIME_S = 'วิ';

	this.GIVE_UP = 'ยอมแพ้';
	this.RESTART = 'เริ่มใหม่';
	this.CONTINUE = 'เล่นต่อ';

	this.ITEM_HAMMER = 'ค้อน';
	this.ITEM_ANGRY = 'โมโห';
	this.ITEM_TIME_STOP = 'หยุดเวลา';

	// ERROR CLIENT
	// connection
	this.ERROR_LOGIN_FAILED = 'เข้าสู่ระบบล้มเหลว';// 4000
	this.ERROR_LOGGED_IN_ANOTHER_DEVICE = 'มีการเข้าสู่ระบบ จากอุปกรณ์อื่น';// 4001
	this.ERROR_TOO_MANY_CONNECTIONS = 'มีผู้ใช้งานจำนวนมาก กรุณารอสักครู่';// 4002
	this.ERROR_LOGIN_AGAIN = 'โปรดเข้าสู่ระบบใหม่อีกครั้ง';// 4003
	// user
	this.ERROR_RETRIEVE_USER_HEART_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลหัวใจของผู้ใช้งาน';// 4100
	this.ERROR_RETRIEVE_USER_ITEMS_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลไอเทมของผู้ใช้งาน';// 4101
	// game play
	this.ERROR_START_GAME = 'มีปัญหาในการเริ่มเกม';// 4200
	this.ERROR_SUBMIT_RESULTS = 'ไม่สามารถส่งผลการเล่นได้';// 4201
	// item shop
	this.ERROR_RETRIEVE_ITEM_SHOP_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลร้านค้า';// 4300
	this.ERROR_PURCHASE = 'การซื้อล้มเหลว';// 4301
	this.ERROR_ITEM_NOT_FOUND = 'ไม่พบไอเทมที่ซื้อ';// 4302
	// ranking
	this.ERROR_RETRIEVE_RANKING_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลอันดับผู้เล่น';// 4400
	this.ERROR_RETRIEVE_USER_RANKING_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลอันดับของผู้ใช้งาน';// 4401
	this.ERROR_RANK_UPDATE = 'เกิดข้อผิดพลาดในการอัปเดตอันดับ';// 4402
	// mail
	this.ERROR_RETRIEVE_MAIL_DATA = 'เกิดข้อผิดพลาด\nในการเรียกข้อมูลจดหมาย';
	// item code
	this.ERROR_REDEEM_ITEM_CODE = 'โค้ดไม่ถูกต้อง' + 'หรือถูกใช้งานแล้ว';

	// ERROR SERVER
	this.ERROR_SERVER = 'ปิดปรับปรุงเซิฟเวอร์';// 5000

	// OPTION PANEL
	this.OPTION_PANEL_HEADER = 'ตัวเลือก';

	// ITEM CODE PANEL
	this.ITEM_CODE_PANEL_HEADER = 'ไอเทมโค้ด';
	this.ITEM_CODE_ENTER_CODE = 'กรุณาใส่โค้ด';
	this.ITEM_CODE_ERROR_INVALID = 'โค้ดไม่ถูกต้อง\n' + 'หรือถูกใช้งานแล้ว';
	this.ITEM_CODE_ERROR_0 = 'โปรดลองใหม่อีกครั้ง\nในภายหลัง';
	this.ITEM_CODE_ERROR_1 = 'โค้ดหมดอายุ';
	this.ITEM_CODE_ERROR_2 = 'การใช้งานโค้ดถึงจำนวนจำกัด\nไม่สามารถใช้งานได้';

	// SHOP PANEL
	this.SHOP_PANEL_HEADER = 'ร้านค้า';
	this.SHOP_PANEL_SUB_HEADER1 = 'เนื้อปลา';
	this.SHOP_PANEL_SUB_HEADER2 = 'ไอเทมช่วยเหลือ ก่อนเริ่มเกม';
	this.SHOP_PANEL_SUB_HEADER3 = 'ไอเทมช่วยเหลือ ระหว่างเกม';
	this.SHOP_PANEL_OFF = 'ลด!';
	this.SHOP_PANEL_OWN = 'มีไอเทมนี้อยู่: ';
	this.SHOP_PANEL_OWN_HEART = 'คุณมีเนื้อปลาอยู่: ';

	// INBOX
	this.INBOX_PANEL_HEADER = 'จดหมาย';
	this.INBOX_PANEL_RECEIVE = 'รับ';
	this.INBOX_PANEL_RECEIVED = 'รับแล้ว';
	this.INBOX_PANEL_RECEIVE_ALL = 'รับทั้งหมด';
	this.INBOX_PANEL_READ = 'อ่าน';
	this.INBOX_PANEL_READV3 = 'อ่านแล้ว';
	this.DELETE_MAIL_PANEL_HEADER = 'ลบจดหมาย';
	this.DELETE_MAIL_PANEL_CONTENT = "ต้องการที่จะลบจดหมาย\n" + "ที่อ่านแล้วทั้งหมดหรือไม่?";

	// ITEM RECEIVED PANEL
	this.ITEM_RECEIVED_PANEL_HEADER = 'คุณได้รับ';

	// INVENTORY PANEL
	this.INVENTORY_PANEL_HEADER = 'ไอเทมของฉัน';

	// DISPLAY_ITEM
	this.DISPLAY_ITEM_HEADER = 'ไอเทมช่วยเหลือ';

	// LB PANEL
	this.LB_PANEL_HEADER = 'อันดับผู้เล่นระดับโลก';
	this.LB_PANEL_PRIZE_HEADER = 'รางวัลระดับโลก';
	this.LB_PANEL_PRIZE_COMMENT = '*อันดับที่ 1 เท่านั้น';

	// LEVEL INFO PANEL
	this.LEVEL_INFO_PANEL_HEADER = 'ด่านที่';
	this.LEVEL_BOSS_INFO_PANEL_HEADER = 'บอส';
	this.LEVEL_INFO_PANEL_BEST_PLAYER = 'ผู้เล่นยอดเยี่ยม';
	this.LEVEL_INFO_PANEL_YOUR_HIGH_SCORE = 'คะแนนสูงสุดของคุณ';
	this.LEVEL_INFO_PANEL_TARGET = 'เป้าหมาย';
	this.LEVEL_INFO_PANEL_REWARD = 'รางวัล';
	this.LEVEL_INFO_PANEL_RECEIVED = 'ได้รับแล้ว';
	this.LEVEL_INFO_PANEL_POINT = 'คูปอง';
	this.LEVEL_INFO_PANEL_TOP3 = '3 ผู้เล่นยอดเยี่ยม';
	this.LEVEL_INFO_PANEL_PRIZE_HEADER = 'รางวัลด่าน';
	this.LEVEL_INFO_PANEL_PRIZE_COMMENT = '*อันดับที่ 1 เท่านั้น';
	this.LEVEL_INFO_PANEL_SELECT_ITEM_HEADER = 'เลือกไอเทม';

	// CANT MOVE PANEL
	this.CANT_MOVE_PANEL_CONTENT = 'เจ้าแมวน้อย ติดแหง็ก';

	// BONUS MOVES PANEL
	this.BONUS_MOVES_PANEL_CONTENT = 'โบนัส อุ้งเท้า';

	// BONUS LEVEL PANEL
	this.BONUS_LEVEL_PANEL_CONTENT = 'โบนัส 3 ดาว';

	// NO HEART PANEL
	this.NO_HEART_PANEL_HEADER = 'เนื้อปลาหมด!';
	this.NO_HEART_PANEL_CONTENT = "เนื้อปลาของคุณหมด\r\n" + "จะเปลี่ยนมาใช้คูปอง";

	// NO POINT PANEL
	this.NO_POINT_PANEL_HEADER = 'ถังแตก!';
	this.NO_POINT_PANEL_CONTENT = "คูปองของคุณหมด\n" + "ไม่สามารถเข้าเล่นได้";

	// GIVE UP PANEL
	this.GIVE_UP_PANEL_HEADER = 'แจ้งเตือน';
	this.GIVE_UP_PANEL_CONTENT = "คุณต้องการออกจากเกม\r\n" + "ที่กำลังเล่น จริงหรือ?";

	// LB ZONE PANEL
	this.LB_ZONE_PANEL_HEADER_PREFIX = 'อันดับผู้เล่น';
	this.LB_ZONE_PANEL_HEADER = 'RANKING ZONE';
	this.LB_ZONE_PANEL_HEADER_SUFFIX = '';

	this.LB_ZONE_PANEL_YOUR_RANK = 'อันดับของคุณ';
	this.LB_ZONE_PANEL_NONE_RANK = 'คุณไม่มีอันดับ';
	this.LB_ZONE_PANEL_TOP_10 = '10 อันดับแรก';
	this.LB_ZONE_PANEL_PRIZE_HEADER = 'รางวัล';
	this.LB_ZONE_PANEL_PRIZE_COMMENT = '*อันดับที่ 1 เท่านั้น';

	// HEART INFO PANEL
	this.HEART_INFO_PANEL_HEADER = 'เนื้อปลา';
	// this.HEART_INFO_PANEL_CONTENT = "ใช้เนื้อปลา 1 ชิ้น ต่อการเข้าเล่น 1
	// ครั้ง\n" + "จันทร์-ศุกร์ จะได้รับทุกๆ 5 ชั่วโมง ต่อ 1 ชิ้น\n" + "(ไม่เกิน
	// 2 ชิ้น)\n";
	this.HEART_INFO_PANEL_CONTENT = "ใช้เนื้อปลา 1 ชิ้น ต่อการเข้าเล่น 1 ครั้ง\n" + "จันทร์-ศุกร์ จะได้รับทุกๆ 5 ชั่วโมง ต่อ 1 ชิ้น\n" + "เสาร์-อาทิตย์ จะได้รับทุกๆ 2 ชั่วโมง ต่อ 1 ชิ้น\n"
			+ "และได้รับ 1 ชิ้น หากชนะครั้งแรกของด่าน\n" + "(ไม่เกิน 2 ชิ้น)";

	// ZONE BOARD
	this.ZONE_BOARD_HEADER = ["ดินแดนลูกกวาด", "ป่าเวทมนต์", "หุบเขาน้ำเเข็ง", "เส้นทาง มรณะ", "โซน 5"];

	this.PRIZE_INFO_PANEL_HEADER = 'เล่นดี มีรางวัล';
	this.PRIZE_INFO_PANEL_TEXT0 = "เงื่อนไขการรับรางวัล";
	this.PRIZE_INFO_PANEL_TEXT1 = "1). ระดับโลก\r\n"
			+ "  ผู้เล่นที่อยู่อันดับที่ 1 ของผู้เล่นระดับโลก (คะแนน\nรวมทุกๆด่านของผู้เล่น) เมื่อจบเวลาการแข่งขัน\nผู้เล่นจะได้รับรางวัลคูปอง ตามจำนวนที่แสดง\n(รางวัลจะเพิ่มขึ้นเรื่อยๆตามจำนวนรอบที่มีการเล่น)\n"
			+ "-------------------------------------------";
	this.PRIZE_INFO_PANEL_TEXT2 = "2). ระดับดินแดน\r\n"
			+ "  ผู้เล่นที่อยู่อันดับที่ 1 ของผู้เล่นระดับดินแดน\n(คะแนนรวมในแต่ละดินดินแดน) เมื่อจบเวลาการ\nแข่งขัน ผู้เล่นจะได้รับรางวัลคูปอง ตามจำนวนที่\nแสดง (รางวัลจะเพิ่มขึ้นเรื่อยๆตามจำนวนที่มีการ\nเล่น ในแต่ละดินแดน)\n"
			+ "-------------------------------------------";
	this.PRIZE_INFO_PANEL_TEXT3 = "3). ระดับด่าน\r\n"
			+ "  ผู้เล่นที่อยู่อันดับที่ 1 ของแต่ละด่าน (คะแนนสูงสุด\nของด่านนั้นๆ) เมื่อจบเวลาการแข่งขัน ผู้เล่นจะได้รับ\nรางวัลคูปอง ตามจำนวนที่แสดง (รางวัลจะเพิ่มขึ้น\nเรื่อยๆตามจำนวนที่มีการเล่น ในด่านนั้นๆ)\n"
			+ "-------------------------------------------";
	// this.PRIZE_INFO_PANEL_PAGE = [ 'จะมีรางวัลในแต่ละด่าน, ดินแดน,
	// ระดับโลก\nซึ่งรางวัลจะเพิ่มขึ้นในแต่ละครั้งที่มีการเล่น',
	// 'เฉพาะผู้เล่นที่ได้ 1 เท่านั้น ที่จะได้รับรางวัล',
	// 'ผู้ชนะจะได้รับรางวัล เมื่อถึงระยะเวลาที่กำหนด' ];
	this.DONT_SHOW_AGAIN = "ไม่ต้องแสดงอีก";

	this.LB_WINNER_LEVEL_HEADER = 'ผู้ชนะระดับด่าน';
	this.LB_WINNER_LEVEL_GRATS = '🎉 ขอแสดงความยินดีกับผู้ชนะทั้ง';
	this.LB_WINNER_LEVEL_WINNERS = 'ท่าน';
	this.LB_WINNER_LEVEL_REWARD_RECEIVED = '*ได้รับรางวัลแล้ว';

	this.MSG_PANEL_HEADER = 'ข้อความ';

	this.NOT_ENOUGH_COUPONS = 'คูปองไม่เพียงพอ';

	// this.ITEMS = {};
	// this.ITEMS['h001'] = 'เนื้อปลา';
	// this.ITEMS['s001'] = 'จรวด';
	// this.ITEMS['s002'] = 'ระเบิด';
	// this.ITEMS['s003'] = 'ดาว';
	// this.ITEMS['s004'] = 'ค้อน';
	// this.ITEMS['s005'] = 'มือ';
	// this.ITEMS['r001'] = 'โลลิป๊อป';
	// this.ITEMS['r002'] = 'ซูชิ';
	// this.ITEMS['r003'] = 'แคทป๊อป';

	this.EA = 'อัน';
	this.PIECE = 'ชิ้น';
	this.COUPONS = 'คูปอง';
	this.MOVES = 'ครั้ง';
	this.TARGET = 'เป้าหมาย';
	this.SCORE = 'คะแนน';
	this.LEVEL = 'ด่าน';
	this.BOSS = 'บอส';
	this.ZONE = 'ดินแดนที่';
	this.COMPLETE = 'ชนะ!';
	this.FAIL = 'แพ้';
	this.OK = 'ตกลง';
	this.YES = 'ใช่';
	this.NO = 'ไม่';
	this.REWARD = 'รางวัล';
	this.RANK = 'อันดับ';
	this.RANKING = 'อันดับ';
	this.NAME = 'รายชื่อ';
	this.CONFIRM = 'ยืนยัน';

	this.END_TIME_PREFIX = 'เหลือเวลา ';
	this.END_TIME_SUFFIX = '';
	this.DAY = 'วัน ';
	this.DAYS = 'วัน ';
	this.END = 'สิ้นสุด ';
}
