/**
 * 
 */

// var LaEN = {
// TEXT_A : 'text A',
// TEXT_B : 'text B'
// };
function LangEN() {

	this.SETTINGS = 'Settings';
	this.BGM = 'MUSIC';
	this.SFX = 'SOUND';
	this.LANG = 'LANGUAGE';

	this.ROOKIE = 'Rookie';
	this.DEMON_SLAYER = 'Demon Slayer';
	this.HASHIRA = 'Hashira';

	this.COMING_SOON = 'COMING SOON';
	this.NOT_ENOUGH_POINTS = 'NOT ENOUGH POINTS';

	this.TANJIRO = 'Tanjiro';
	this.ZENITSU = 'Zenitsu';

	this.CHARACTER = 'Character';
	this.MONSTERS = 'Monsters';

	this.BONUS = 'BONUS';
	this.BOSS = 'BOSS';

	this.AUTO = 'AUTO';
	this.ITEM_CODE = 'Item Code';
	this.ITEM_CODE_HEADER = 'ITEM CODE';

	this.LUCKY_CODE = 'Lucky Code';
	this.LUCKY_CODE_HEADER = 'LUCKY CODE';

	this.BONUS_TIME = 'BONUS TIME!';

	// btn
	this.SETTING = 'Setting';
	this.EXIT = 'Exit';
	this.BACK = 'Back';

	this.PANEL_ALERT_HEADER = 'ALERT';

	this.PANEL_TUTORIAL_HEADER = 'TUTORIAL';
	this.TUTORIAL_HOW_TO_PLAY = {
		header : 'HOT TO PLAY',
		desc : 'Pop the bubbles,\nmatch three or more of them.'
	};
	this.TUTORIAL_ANGRY_GAUGE = {
		header : 'ANGRY GAUGE',
		desc : 'Fill the Angry Gauge,\nget at least x3 combo.'
	};
	this.TUTORIAL_ANGRY_GAUGE_MAX = {
		header : "I'M ANGRY!",
		desc : 'When fully enraged, press angry icon\nto get fire ball that deals damage\naround the point of impact.'
	};
	this.TUTORIAL_ANGEL_GOOD = {
		header : 'ANGEL',
		desc : 'Make a wish and send it to the Angel,\nShoot to get a booster.'
	};
	this.TUTORIAL_ANGEL_DARK = {
		header : 'DARK ANGEL',
		desc : 'Shoot the Dark Angel!\nto stop spawning new type of bubble.'
	};
	this.TUTORIAL_BOOSTER_X2_SCORE = {
		header : 'X2 SCORE',
		desc : 'Double your score points.'
	};
	this.TUTORIAL_BOOSTER_AIM_ASSIST = {
		header : 'AIM ASSIST',
		desc : 'Show aiming line.'
	};
	this.TUTORIAL_BOOSTER_PUSH = {
		header : 'PUSH',
		desc : 'Push falling bubbles back.'
	};

	this.WORLD_RANKING = 'Top 100 Ranking';
	this.TOP100_RANKING = 'Top 100';

	this.RANKING_REWARDS = 'Ranking Rewards';

	this.MY_RANK = 'My Rank';

	this.DAILY = 'Daily';
	this.WEEKLY = 'Weekly';
	this.MONTHLY = 'Monthly';

	this.DAILY_RANKING = 'Daily Ranking';
	this.WEEKLY_RANKING = 'Weekly Ranking';
	this.MONTHLY_RANKING = 'Monthly Ranking';

	this.DAILY_RANKING_REWARDS = 'Daily Rewards';
	this.WEEKLY_RANKING_REWARDS = 'Weekly Rewards';
	this.MONTHLY_RANKING_REWARDS = 'Monthly Rewards';

	this.PANEL_GAME_OVER_HEADER = 'GAME OVER';

	this.PANEL_RECEIVED_ITEM_HEADER = 'Received';

	this.PANEL_CONFIRM_PURCHASE_HEADER = 'CONFIRMATION';
	this.PANEL_CONFIRM_PURCHASE_CONTENT = 'Purchase';

	this.PANEL_SETTINGS_HEADER = 'SETTINGS';

	this.PANEL_PAUSE_HEADER = 'PAUSE';

	this.PANEL_REDEEM_CODE_HEADER = 'ITEM CODE';

	this.LEADERBOARD = 'RANKING';

	this.NOT_ENOUGH_HEART = 'Not enough heart';
	this.GO_TO_SHOP = 'Go to Shop';

	this.ITEM_INFO_002 = 'Filled angry gauge to MAX';
	this.ITEM_INFO_003 = 'Time stopped for 5 seconds';
	this.ITEM_INFO_004 = 'Destroy all bubbles';

	// BTN
	this.PLAY = 'PLAY';
	this.SHOP = 'Shop';
	this.INVENTORY = 'Inventory';

	this.REDEEM = 'Redeem';
	this.REDEEM_CODE = 'Redeem';
	this.MAIL = 'Mail';
	this.HEART = 'Heart';
	this.ITEM = 'Item';
	this.CONFIRM = 'Confirm';
	this.MAX = 'MAX';
	this.RANK = 'RANK';
	this.BEST = 'BEST';
	this.FREE = 'FREE';
	this.OWN = 'OWN';
	this.GET = 'GET';
	this.EXPIRED = 'EXPIRED';
	this.CANCEL = 'CANCEL';

	this.TIME_D = 'd';
	this.TIME_H = 'h';
	this.TIME_M = 'm';
	this.TIME_S = 's';

	this.GIVE_UP = 'Give up';
	this.RESTART = 'Restart';
	this.CONTINUE = 'Continue';

	this.ITEM_HAMMER = 'Hammer';
	this.ITEM_ANGRY = 'Angry';
	this.ITEM_TIME_STOP = 'Time Stop';

	// ERROR CLIENT
	// connection
	this.ERROR_LOGIN_FAILED = 'Login failed';// 4000
	this.ERROR_LOGGED_IN_ANOTHER_DEVICE = 'Your account has been logged in another device';// 4001
	this.ERROR_TOO_MANY_CONNECTIONS = 'Too many connections, please wait...';// 4002
	this.ERROR_LOGIN_AGAIN = 'Your game session has expired,\nPlease login again';// 4003
	// user
	this.ERROR_RETRIEVE_USER_HEART_DATA = 'Failed to retrieve heart data';// 4100
	this.ERROR_RETRIEVE_USER_ITEMS_DATA = 'Failed to retrieve items data';// 4101
	// game play
	this.ERROR_START_GAME = 'Failed to start game';// 4200
	this.ERROR_SUBMIT_RESULTS = 'Failed to submit results';// 4201
	// item shop
	this.ERROR_RETRIEVE_ITEM_SHOP_DATA = 'Failed to retrieve item shop data';// 4300
	this.ERROR_PURCHASE = 'Purchase failed';// 4301
	this.ERROR_ITEM_NOT_FOUND = 'Item not found';// 4302
	// ranking
	this.ERROR_RETRIEVE_RANKING_DATA = 'Failed to retrieve ranking data';// 4400
	this.ERROR_RETRIEVE_USER_RANKING_DATA = 'Failed to retrieve user ranking data';// 4401
	this.ERROR_RANK_UPDATE = 'Rank update error';// 4402
	// mail
	this.ERROR_RETRIEVE_MAIL_DATA = 'Failed to retrieve mail data';
	// item code
	this.ERROR_REDEEM_ITEM_CODE = 'This code is invalid ' + 'or redeemed';

	// ERROR SERVER
	this.ERROR_SERVER = 'Server Maintenance';// 5000

	// OPTION PANEL
	this.OPTION_PANEL_HEADER = 'OPTION';

	// ITEM CODE PANEL
	this.ITEM_CODE_PANEL_HEADER = 'ITEM CODE';
	this.ITEM_CODE_ENTER_CODE = 'Enter your code';
	this.ITEM_CODE_ERROR_INVALID = 'This code is invalid\n' + 'or redeemed';
	this.ITEM_CODE_ERROR_0 = 'Try again later';
	this.ITEM_CODE_ERROR_1 = 'Code has expired';
	this.ITEM_CODE_ERROR_2 = 'Code usage limit\n' + 'reached!';

	// SHOP PANEL
	this.SHOP_PANEL_HEADER = 'SHOP';
	this.SHOP_PANEL_SUB_HEADER1 = 'FISH STEAK';
	this.SHOP_PANEL_SUB_HEADER2 = 'PRE-GAME BOOSTER';
	this.SHOP_PANEL_SUB_HEADER3 = 'IN-GAME BOOSTER';
	this.SHOP_PANEL_OFF = 'OFF!';

	// INBOX
	this.INBOX_PANEL_HEADER = 'INBOX';
	this.INBOX_PANEL_RECEIVE = 'Receive';
	this.INBOX_PANEL_RECEIVED = 'Received';
	this.INBOX_PANEL_RECEIVE_ALL = 'Receive all';
	this.INBOX_PANEL_READ = 'Read';
	this.INBOX_PANEL_READV3 = 'Read';
	this.DELETE_MAIL_PANEL_HEADER = 'DELETE MAILS';
	this.DELETE_MAIL_PANEL_CONTENT = "Are you sure to delete\n" + "all read mails?";

	// ITEM RECEIVED PANEL
	this.ITEM_RECEIVED_PANEL_HEADER = 'You have received';

	// INVENTORY PANEL
	this.INVENTORY_PANEL_HEADER = 'INVENTORY';

	// DISPLAY_ITEM
	this.DISPLAY_ITEM_HEADER = 'BOOSTER ITEM';

	// LB PANEL
	this.LB_PANEL_HEADER = 'WORLD RANKING';
	this.LB_PANEL_PRIZE_HEADER = 'WORLD PRIZE';
	this.LB_PANEL_PRIZE_COMMENT = '*NO.1 only';

	// LEVEL INFO PANEL
	this.LEVEL_INFO_PANEL_HEADER = 'LEVEL';
	this.LEVEL_BOSS_INFO_PANEL_HEADER = 'BOSS';
	this.LEVEL_INFO_PANEL_BEST_PLAYER = 'BEST PLAYER';
	this.LEVEL_INFO_PANEL_YOUR_HIGH_SCORE = 'YOUR HIGH SCORE';
	this.LEVEL_INFO_PANEL_TARGET = 'TARGET';
	this.LEVEL_INFO_PANEL_REWARD = 'REWARD';
	this.LEVEL_INFO_PANEL_RECEIVED = 'RECEIVED';
	this.LEVEL_INFO_PANEL_POINT = 'COUPON';
	this.LEVEL_INFO_PANEL_TOP3 = 'TOP 3';
	this.LEVEL_INFO_PANEL_PRIZE_HEADER = 'LEVEL PRIZE';
	this.LEVEL_INFO_PANEL_PRIZE_COMMENT = '*NO.1 only';
	this.LEVEL_INFO_PANEL_SELECT_ITEM_HEADER = 'SELECT BOOSTERS';

	// CANT MOVE PANEL
	this.CANT_MOVE_PANEL_CONTENT = 'A poor kitty got stuck!';

	// BONUS MOVES PANEL
	this.BONUS_MOVES_PANEL_CONTENT = 'BONUS PAW';

	// BONUS LEVEL PANEL
	this.BONUS_LEVEL_PANEL_CONTENT = '3 STARS BONUS';

	// NO HEART PANEL
	this.NO_HEART_PANEL_HEADER = 'NO FISH STEAK!';
	this.NO_HEART_PANEL_CONTENT = "Your fish steak run out!\r\n" + "use Coupon instead";

	// NO POINT PANEL
	this.NO_POINT_PANEL_HEADER = 'NO COUPONS!';
	this.NO_POINT_PANEL_CONTENT = "You dont have any coupons\n" + "Cant play";

	// GIVE UP PANEL
	this.GIVE_UP_PANEL_HEADER = 'QUIT';
	this.GIVE_UP_PANEL_CONTENT = "Are you sure you want\r\n" + "to quit this game?";

	// LB ZONE PANEL
	this.LB_ZONE_PANEL_HEADER_PREFIX = '';
	this.LB_ZONE_PANEL_HEADER = 'RANKING ZONE';
	this.LB_ZONE_PANEL_HEADER_SUFFIX = 'RANKING';

	// this.LB_ZONE_PANEL_HEADER_FIX = 'RANKING ZONE';
	this.LB_ZONE_PANEL_YOUR_RANK = 'YOUR RANK';
	this.LB_ZONE_PANEL_NONE_RANK = 'NO YOUR RANK';
	this.LB_ZONE_PANEL_TOP_10 = 'TOP 10';
	this.LB_ZONE_PANEL_PRIZE_HEADER = 'PRIZE';
	this.LB_ZONE_PANEL_PRIZE_COMMENT = '*NO.1 only';

	// HEART INFO PANEL
	this.HEART_INFO_PANEL_HEADER = 'FISH STEAK';
	// this.HEART_INFO_PANEL_CONTENT = "Use 1 fish steak per play a level\n" +
	// "restore 1 every 5 hours\n" + "(limit 2)\n";
	this.HEART_INFO_PANEL_CONTENT = "Use 1 fish steak per play a level\n" + "Mon - Fri restore 1 every 5 hours\n" + "Sat - Sun restore 1 every 2 hours\n"
			+ "Receive 1, if you win a level first time\n" + "(limit 2)";

	// ZONE BOARD
	this.ZONE_BOARD_HEADER = ["CANDY LAND", "MAGIC FOREST", "ICE CANYON", "DEADLY ROAD", "ZONE 5"];

	this.PRIZE_INFO_PANEL_HEADER = "Every Kitty's Dream";
	this.PRIZE_INFO_PANEL_TEXT0 = "Reward conditions";
	this.PRIZE_INFO_PANEL_TEXT1 = "1). World Reward\r\n"
			+ "  World\'s # 1 ranked player (Total score for\nall stages) At the end of the time, player\nwill receive coupons as shown (The PRIZE\nwill increase by the number of rounds\nplayed)\n"
			+ "-------------------------------------------";
	this.PRIZE_INFO_PANEL_TEXT2 = "2). Zone Reward\r\n"
			+ "  Zone\'s # 1 ranked player each zone (Total\nscore in each zone) At the end of the time,\nplayer will receive coupons as shown (The\nPRIZE will increase by the number of rounds\nplayed in each zone)\n"
			+ "-------------------------------------------";
	this.PRIZE_INFO_PANEL_TEXT3 = "3). Level Reward\r\n"
			+ "  Level\'s # 1 ranked player each level (High\nscore of that level) At the end of the time,\nplayer will receive coupons as shown (The \nPRIZE will increase by the number of rounds\nplayed in each level)\n"
			+ "-------------------------------------------";
	// this.PRIZE_INFO_PANEL_PAGE = [ 'Rewards split up each level, zone,
	// world\nWhich raises prize from players playing', 'The rank 1 player will
	// get reward',
	// 'Winner will get reward at the end of time' ];
	this.DONT_SHOW_AGAIN = "don't show again";

	this.LB_WINNER_LEVEL_HEADER = 'WINNERS of \"LEVEL\"';
	this.LB_WINNER_LEVEL_GRATS = '🎉 Congratulations to top';
	this.LB_WINNER_LEVEL_WINNERS = 'winners';
	this.LB_WINNER_LEVEL_REWARD_RECEIVED = '*Reward received';

	this.MSG_PANEL_HEADER = 'MESSAGE';

	this.NOT_ENOUGH_COUPONS = 'Not enough coupons';

	this.EA = 'ea';
	this.PIECE = 'FISH STEAK';
	this.COUPONS = 'COUPONS';
	this.MOVES = 'MOVES';
	this.TARGET = 'TARGET';
	this.SCORE = 'SCORE';
	this.LEVEL = 'LEVEL';

	this.ZONE = 'ZONE';
	this.COMPLETE = 'COMPLETE!';
	this.FAIL = 'FAIL';
	this.OK = 'OK';
	this.YES = 'YES';
	this.NO = 'NO';
	this.REWARD = 'REWARD';
	this.RANKING = 'RANKING';
	this.NAME = 'NAME';

	this.END_TIME_PREFIX = 'TIME REMAINING ';
	this.END_TIME_SUFFIX = '';
	this.DAY = 'DAY ';
	this.DAYS = 'DAYS ';
	this.END = 'END ';
}
