var PIVOT = {
	TOP_LEFT : 'TOP_LEFT',
	MIDDLE : 'MIDDLE'
};

var ALIGN = {
	LEFT : 'LEFT',
	CENTER : 'CENTER'
};

var Util = {

	loadUserPicture : function(idx, url, mask, group, context) {
		if (gameInstance.cache.checkImageKey(idx)) {
			Util.onLoadUserPictureCompleted(idx, mask, group, context);
		} else {
			if (url != null && url != '') {
				gameInstance.load.image(idx, url);
				gameInstance.load.onLoadComplete.addOnce(Util.onLoadUserPictureCompleted, this, 0, idx, mask, group, context);
				gameInstance.load.crossOrigin = "anonymous";
				gameInstance.load.start();
			}
		}

	},

	onLoadUserPictureCompleted : function(idx, mask, group, context) {
		if (context == null)
			return;

		var image = gameInstance.add.sprite(0, 0, idx);
		image.visible = false;

		var nSize = Util.fixUserPictureScale(image, mask);
		var bmd = gameInstance.make.bitmapData(mask.width, mask.height);
		var midX = (mask.width / 2) - (nSize.w / 2);
		var midY = (mask.height / 2) - (nSize.h / 2);
		var originalMaskW = mask.width / mask.scale.x;
		var originalMaskH = mask.height / mask.scale.y;
		var rect1 = new Phaser.Rectangle(midX, midY, nSize.w, nSize.h);
		var rect2 = new Phaser.Rectangle(0, 0, originalMaskW, originalMaskH);
		bmd.alphaMask(image, mask, rect1, rect2);

		var bmdImage = gameInstance.add.sprite(0, 0, bmd);
		group.add(bmdImage);
		bmdImage.x = mask.x;
		bmdImage.y = mask.y;
	},

	fixUserPictureScale : function(image, mask) {
		var nh = 0;
		var nw = 0;

		nh = mask.height;
		nw = (mask.height * image.width) / image.height;

		if (nh < mask.height || nw < mask.width) {
			nh = (mask.width * image.height) / image.width;
			nw = mask.width;
		}

		return {
			w : nw,
			h : nh
		};
	},

	pad : function(num, size) {
		var s = num + "";
		while (s.length < size)
			s = "0" + s;
		return s;
	},

	numberWithCommas : function(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	},

	randomMinMax : function(min, max) {
		var nMin = Math.ceil(min);
		var nMax = Math.floor(max);
		return Math.floor(Math.random() * (nMax - nMin + 1) + nMin);
		// The maximum is inclusive and the minimum is inclusive
	},

	distance : function(current, target) {
		var a = current.x - target.x;
		var b = current.y - target.y;
		var c = Math.floor(Math.sqrt(a * a + b * b));
		return c;
	},

	getDistanceBetweenTwoPoints : function(startX, startY, endX, endY) {
		var a = startX - endX;
		var b = startY - endY;
		var c = Math.floor(Math.sqrt(a * a + b * b));
		return c;
	},

	averageWidth : function(array, offset, pivot, align) {
		//		
		var maxW = 0;
		for (var i = 0; i < array.length; i++) {
			maxW += array[i].width;
		}
		maxW += offset * (array.length - 1);

		var dist = array[0].width + offset;
		for (var i = 0; i < array.length; i++) {
			array[i].x = dist * i;
		}

		switch (align) {
			case ALIGN.LEFT :
				break;
			case ALIGN.CENTER :
				for (var i = 0; i < array.length; i++) {
					array[i].x -= maxW / 2;
				}
				break;
		}

	},

	averageX : function(array, offset) {
		//		
		var maxW = 0;
		for (var i = 0; i < array.length; i++) {
			maxW += array[i].width;
		}
		maxW += offset * (array.length - 1);

		array[0].x = -(maxW / 2) + (array[0].width / 2);
		var nextPos = array[0].x + (array[0].width / 2);
		for (var i = 1; i < array.length; i++) {
			array[i].x = nextPos + (array[i].width / 2) + offset;

			nextPos = array[i].x + (array[i].width / 2);
		}
	},

	kFormat : function(num) {
		if (Math.abs(num) > 999999) {
			return Math.sign(num) * ((Math.abs(num) / 1000000).toFixed(2)) + 'M';
		} else if (Math.abs(num) > 999) {
			return Math.sign(num) * ((Math.abs(num) / 1000).toFixed(1)) + 'K';
		}

		return Math.sign(num) * Math.abs(num);
	}

};
