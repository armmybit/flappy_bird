var _this;
var _game;

function RequestAPI(game) {
	_game = game;
	_this = this;
}

RequestAPI.prototype.login = function(callback) {
	var request = new XMLHttpRequest();
	request.open('POST', _game.API.USERS, true);
	request.setRequestHeader('Content-type',
			'application/x-www-form-urlencoded; charset=UTF-8');
	request.onload = function() {
		console.log(this.response);
		var data = JSON.parse(this.response);
		console.log("[API-UserDetails-Response]←");
		console.log('• user_name: ' + data['user_name']);
		console.log('• utf8_name: ' + data['user_name']);
		console.log('• user_logo: ' + data['user_logo']);

		callback();
	};
	request.send('user_id=' + _game.userDetails['user_id'] + '&user_name='
			+ _game.userDetails['user_name'] + '&user_logo='
			+ _game.userDetails['user_logo']);
};

RequestAPI.prototype.postScoreToLeaderboard = function(score) {
	var request = new XMLHttpRequest();
	request.open('POST', _game.API.LEADERBOARDS, true);
	request.setRequestHeader('Content-type',
			'application/x-www-form-urlencoded; charset=UTF-8');
	request.onload = function() {
		console.log(this.response);
		// var data = JSON.parse(this.response);
		console.log("[API-PostScoreToLeaderboard-Response]←");
	};
	request.send('user_id=' + _game.userDetails['user_id'] + '&user_name='
			+ _game.userDetails['user_name'] + '&user_logo='
			+ _game.userDetails['user_logo'] + '&score=' + score);
};

RequestAPI.prototype.getLeaderboard = function(callback) {
	var request = new XMLHttpRequest();
	request.open('POST', _game.API.LEADERBOARDS, true);
	request.setRequestHeader('Content-type',
			'application/x-www-form-urlencoded; charset=UTF-8');
	request.onload = function() {
		console.log(this.response);
		var data = JSON.parse(this.response);
		callback(data);
		console.log("[API-GetLeaderboard-Response]←");

	};
	request.send('user_id=' + _game.userDetails['user_id']);
};
