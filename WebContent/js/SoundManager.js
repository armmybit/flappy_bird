/**
 * 
 */

var BGM = {
	GAME : 'bgm_game'
};

var SFX = {
	BTN : "btn",
	CAT : "cat",
	END : "end",
	HIT_PIPE : "hit_pipe",
	HIT : 'hit',
	JUMP : "jump",
	SCORE : "score"

};

var SM = {
	bgmVolume : 0,
	sfxVolume : 0,
	bgm : null,
	curBGMName : '',
	twBgm : null,
	skillBgm : null,
	curSkillBgmName : '',
	twSkillBgm : null,

	playSFX : function(sfxName) {
		var selectedSFX = sfxName;
		// if (sfxName == SFX.SLASH) {
		// var slashs = [ SFX.SLASH1, SFX.SLASH2, SFX.SLASH3, SFX.SLASH4 ];
		// selectedSFX = slashs[Math.floor(Math.random() * slashs.length)];
		//
		// }
		gameInstance.add.audio(selectedSFX).play('', 0, SM.sfxVolume);
	},

	playBGM : function(bgmName) {
		if (SM.bgm == null) {
			SM.bgm = gameInstance.add.audio(bgmName);
			SM.curBGMName = bgmName;
			gameInstance.sound.setDecodedCallback(SM.bgm, SM.onBGMReady, this);
		} else {
			if (SM.curBGMName != bgmName) {
				SM.fadeOut(bgmName);
				SM.curBGMName = bgmName;
			}
		}
	},

	clearSkillBGM : function() {
		if (SM.twSkillBgm != null) {
			SM.twSkillBgm.stop();
			gameInstance.tweens.remove(SM.twSkillBgm);
			SM.twSkillBgm = null;
		}

		if (SM.twBgm != null) {
			SM.twBgm.stop();
			gameInstance.tweens.remove(SM.twBgm);
			SM.twBgm = null;
		}

		if (SM.skillBgm != null) {
			SM.skillBgm.stop();
			SM.skillBgm = null;
		}
	},

	playSkillBGM : function(bgmName) {
		// if(SM.bgm != null){
		// gameInstance.add.tween(SM.bgm).to({
		// volume : 0
		// }, 500, Phaser.Easing.Cubic.Out, true);
		// }
		if (SM.twSkillBgm != null) {
			SM.twSkillBgm.stop();
			gameInstance.tweens.remove(SM.twSkillBgm);
			SM.twSkillBgm = null;
		}

		if (SM.twBgm != null) {
			SM.twBgm.stop();
			gameInstance.tweens.remove(SM.twBgm);
			SM.twBgm = null;
		}

		if (SM.skillBgm != null) {
			SM.skillBgm.stop();
			SM.skillBgm = null;
		}

		SM.setBGMVolumeTemp(0);

		// if (SM.skillBgm == null) {
		SM.skillBgm = gameInstance.add.audio(bgmName);
		SM.curSkillBgmName = bgmName;
		// gameInstance.sound.setDecodedCallback(SM.skillBgm, SM.onBGMReady,
		// this);
		SM.skillBgm.play('', 0, SM.sfxVolume, true);
		// }
		// else {
		// if (SM.curBGMName != bgmName) {
		// SM.fadeOut(bgmName);
		// SM.curBGMName = bgmName;
		// }
		// }
	},

	fadeOutSkillBGM : function(duration) {
		SM.twSkillBgm = gameInstance.add.tween(SM.skillBgm).to({
			volume : 0
		}, duration, Phaser.Easing.Cubic.Out);
		SM.twSkillBgm.start();

	},

	fadeInBGM : function(duration) {
		SM.twBgm = gameInstance.add.tween(SM.bgm).to({
			volume : SM.bgmVolume
		}, duration, Phaser.Easing.Cubic.Out);
		SM.twBgm.start();
	},

	onBGMReady : function() {
		SM.bgm.play('', 0, SM.bgmVolume, true);
	},

	fadeOut : function(bgmName) {
		gameInstance.add.tween(SM.bgm).to({
			volume : 0
		}, 500, Phaser.Easing.Cubic.Out, true).onComplete.add(SM.fadeIn, this, 0, bgmName);
	},

	fadeIn : function(a, b, bgm) {
		SM.bgm.stop();
		SM.bgm = null;
		SM.bgm = gameInstance.add.audio(bgm);
		gameInstance.sound.setDecodedCallback(this.bgm, this.onFadeInReady, this);
	},
	onFadeInReady : function() {
		SM.bgm.play('', 0, 0, true);
		gameInstance.add.tween(SM.bgm).to({
			volume : SM.bgmVolume
		}, 500, Phaser.Easing.Cubic.Out, true);
	},

	setBGMVolumeTemp : function(volume) {
		SM.bgm.volume = volume;
	},

	setBGMVolume : function(volume) {
		if (SM.bgm != null) {
			SM.bgm.volume = volume;
		}

		SM.bgmVolume = volume;
		localStorage.setItem(KEYS.BGM_VOLUME, volume);
	},

	setSFXVolume : function(volume) {
		SM.sfxVolume = volume;
		localStorage.setItem(KEYS.SFX_VOLUME, volume);
	}
};

// bgm
if (localStorage.getItem(KEYS.BGM_VOLUME) == null) {
	localStorage.setItem(KEYS.BGM_VOLUME, 1);
}
SM.bgmVolume = parseFloat(localStorage.getItem(KEYS.BGM_VOLUME));

// sfx
if (localStorage.getItem(KEYS.SFX_VOLUME) == null) {
	localStorage.setItem(KEYS.SFX_VOLUME, 1);
}
SM.sfxVolume = parseFloat(localStorage.getItem(KEYS.SFX_VOLUME));
